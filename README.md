Banque d'exercices de colles pour les CPI1.

La structure du repo est un peu spéciale : 

* `colles.tex` est seulement le fichier qui va inclure tous les exos.
* `contents/` contient les .tex qui eux-même contiennent les exercices.
* `contents/custom.tex` contient les changements d'options (un peu crade, désolé).

Ajouter un exercice est très facile, simplement regarder comment sont faits les autres.

Pour compiler :

```{sh}
$ pdflatex colles.tex
```